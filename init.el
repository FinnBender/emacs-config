;;; init -- init file -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:
;;;; straight.el
(setq package-enable-at-startup nil)
(defvar bootstrap-version)
(defvar straight-use-package-by-default t)
(defvar straight-check-for-modification 'live-with-find)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq use-package-expand-minimally t)
(defvar use-package-compute-statistics t)
;;;; fundamental
(use-package no-littering) ;; https://github.com/emacscollective/no-littering
(use-package blackout ;; https://github.com/raxod502/blackout
  :demand t)
(use-package general) ;; https://github.com/noctuid/general.el
(use-package emacs
  :demand t
  :blackout whitespace-mode
  :preface
  (defun whitespace-setup (&rest ignored)
    "Setup whitespace-mode"
    (whitespace-mode t)
    (general-add-hook 'before-save #'whitespace-cleanup))
  :custom
  (auto-save-default nil)
  (backward-delete-char-untabify-method nil)
  (bidi-inhibit-bpa t)
  (bidi-paragraph-direction 'left-to-right)
  (comint-prompt-read-only t)
  (display-line-numbers-type 'relative)
  (initial-major-mode 'org-mode)
  (initial-scratch-message "* Scratch\n# This buffer is for text that is not saved.\n\n")
  (inhibit-startup-screen t)
  (make-backup-files nil)
  (max-mini-window-height 0.2)
  (native-comp-async-report-warnings-errors nil)
  (package-native-compile t)
  (redisplay-dont-pause t)
  (show-paren-delay 0)
  (switch-to-buffer-obey-display-actions t)
  (text-scale-mode-step 1.1)
  (vc-follow-symlinks nil)
  (whitespace-style '(face spaces space-mark tabs tab-mark trailing indentation))
  (window-divider-default-bottom-width 1)
  (window-divider-default-places t)
  (window-divider-default-right-width 1)
  (x-gtk-resize-child-frames 'hide)

  :init
  (add-to-list 'default-frame-alist '(fullscreen . maximized))
  (add-to-list 'display-buffer-alist
               '("\\*.*\\*"
                 (display-buffer-in-side-window)
                 (side . left)
                 (window-width . 90)))
  ;; (set-frame-font "Source Code Pro-11" nil t)
  (set-frame-font "Iosevka-12" nil t)
  (scroll-bar-mode 0) ; Disable scrollbar
  (tool-bar-mode 0)   ; Disable tool bar
  (tooltip-mode 0)    ; Disable tooltips
  (menu-bar-mode 0)   ; Disable the menu bar
  (set-fringe-mode 10)  ; Give some breathing room
  (window-divider-mode t)

  :config
  (global-display-line-numbers-mode t)

  (setq-default indent-tabs-mode nil)
  (setq-default tab-always-indent nil)
  (setq-default tab-width 4)
  (setq-default word-wrap t)

  (delete-selection-mode t)
  (savehist-mode t)

  (set-register ?e '(file . "~/.emacs.d/init.el"))
  (set-register ?t '(file . "~/.emacs.d/templates"))

  (add-to-list 'vc-directory-exclusion-list "dist-newstyle")

  :hook
  (fundamental-mode . whitespace-setup)
  (prog-mode . whitespace-setup)
  (text-mode . whitespace-setup)

  (pdf-view-mode . auto-revert-mode)

  :general
  ("DEL" 'backward-delete-char-untabify)
  ("C-c s r" 'raise-sexp))

;;;; packages
(use-package agda2-mode ;; https://github.com/agda/agda/blob/master/src/data/emacs-mode/agda2-mode.el
  :straight (:type git :host github :repo "agda/agda"
                   :branch "release-2.6.2.2"
                   :files ("src/data/emacs-mode/*.el"))
  :custom
  (default-input-method "Agda")
  (input-method-verbose-flag nil))
(use-package almost-mono-theme
  :straight (:type git :host github :repo "Fybe/almost-mono"
                   :local-repo "~/Programming/almost-mono-themes")
  :demand t
  :config
  (load-theme 'almost-mono t))
(use-package ansi-color
  :init
  (defun colorize-compilation-buffer ()
    (ansi-color-apply-on-region compilation-filter-start (point)))
  :hook
  (compilation-filter . colorize-compilation-buffer))
(use-package cape ;; https://github.com/minad/cape
  :demand t
  ;; :init
  ;; (add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;; (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  ;; (add-to-list 'completion-at-point-functions #'cape-file)
  )
(use-package compile
  :straight (:type built-in)
  :config
  (add-to-list
   'compilation-error-regexp-alist-alist
   '(rust-panic
     "^[ ]+at \\(\\./\\([^:]+\\):\\([0-9]+\\):\\([0-9]+\\)\\)$"
     2 3 4 nil 1))
  (add-to-list 'compilation-error-regexp-alist 'rust-panic)
  (add-to-list
   'compilation-error-regexp-alist-alist
   '(haskell-haddock
     "^[ ]+\\([^ ]+ (\\([^:]+\\):\\([0-9]+\\))\\)$"
     2 3 nil 1 1))
  (add-to-list 'compilation-error-regexp-alist 'haskell-haddock))
(use-package consult ;; https://github.com/minad/consult
  :general
  ("C-x b" 'consult-buffer)                ;; orig. switch-to-buffer
  ("C-x 4 b" 'consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
  ("C-x 5 b" 'consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
  ("C-x p b" 'consult-project-buffer)      ;; orig. project-switch-to-buffer
  ("M-g e" 'consult-compile-error)
  ("M-g f" 'consult-flymake)
  ("M-g i" 'consult-imenu-multi)
  ("M-r" 'consult-isearch-history)
  (:keymaps 'minibuffer-local-map
            "M-r" 'consult-history)
  :init
  ;; (advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  :config
  (consult-customize consult-theme
                     :preview-key '(:debounce 0.2 any)
                     consult-ripgrep consult-git-grep consult-grep
                     consult-bookmark consult-recent-file consult-xref
                     consult--source-bookmark consult--source-recent-file
                     consult--source-project-recent-file))
(use-package corfu ;; https://github.com/minad/corfu
  :straight (:files (:defaults "extensions/*.el"))
  :init
  (require 'corfu-popupinfo)
  :custom
  (corfu-count 10)
  (corfu-min-width 20)
  (corfu-auto nil)
  (corfu-quit-at-boundary t)
  (corfu-on-exact-match nil)
  (corfu-preview-current t)
  (corfu-preselect 'prompt)
  (tab-first-completion 'word)
  :config
  (global-corfu-mode t)
  (corfu-popupinfo-mode t)

  (defun my/capf ()
    (list
     (cape-super-capf
        #'tempel-complete
        #'cape-dabbrev
        (cape-capf-inside-string #'cape-file))))

  (defun corfu-setup-capf ()
    (setq-local completion-at-point-functions (my/capf)))
  :hook
  (prog-mode . corfu-setup-capf)
  (eglot-managed-mode . corfu-setup-capf)
  :general
  ("C-TAB" 'corfu-complete)
  (:keymaps 'corfu-map
            "M-d" 'corfu-popupinfo-toggle))
(use-package ctrlf ;; https://github.com/radian-software/ctrlf
  :config
  (ctrlf-mode 1))
(use-package dart-mode ;; https://github.com/bradyt/dart-mode
  )
(use-package dired
  :straight (:type built-in)
  :general
  (:keymaps 'dired-mode-map
            "C-c C-c" 'dired-toggle-read-only)
  :custom
  (dired-listing-switches "-agho --group-directories-first")
  :hook
  (dired-mode . auto-revert-mode))
(use-package dired-single ;; https://github.com/crocket/dired-single
  :general
  (:keymaps 'dired-mode-map
            [remap dired-find-file] 'dired-single-buffer
            [remap dired-mouse-find-file] 'dired-single-buffer-mouse
            [remap dired-up-directory] 'dired-single-up-directory))
(use-package dired-hide-dotfiles ;; https://github.com/mattiasb/dired-hide-dotfiles/blob/master/dired-hide-dotfiles.el
  :hook
  (dired-mode . dired-hide-dotfiles-mode)
  :custom
  (dired-hide-dotfiles-verbose nil)
  :general
  (:keymaps 'dired-mode-map
            "C-c ." 'dired-hide-dotfiles-mode))
(use-package bnf-mode) ;; https://github.com/sergeyklay/bnf-mode
(use-package editorconfig ;;  https://github.com/editorconfig/editorconfig-emacs
  :blackout
  :config
  (editorconfig-mode t))
(use-package eglot ;; https://github.com/joaotavora/eglot
  :defer t
  :functions 'eglot-ensure
  :preface
  (defun eglot-setup ()
    (eglot-inlay-hints-mode -1))
  :custom
  (eglot-autoshutdown t)
  (eglot-sync-connect 0)
  (eglot-extend-to-xref t)
  (eglot-confirm-server-initiated-edits nil)
  (eglot-ignored-server-capabilities '(:documentHighlightProvider))
  :init
  (setq-default eglot-workspace-configuration
                '((pylsp (plugins (pycodestyle (select . ["B950"])
                                               (ignore . ["E203" "E501"]))
                                  (black (enabled . t)
                                         (preview . t))
                                  (yapf (enabled . :json-false))))
                  (haskell (formattingProvider . "fourmolu")
                           (maxCompletions . 50)
                           (plugin (hlint (globalOn . :json-false))))
                  ))
  :hook
  (eglot-managed-mode . eglot-setup)
  :general
  (:keymaps 'eglot-mode-map
            "C-c r" 'eglot-rename
            "C-c o" 'eglot-code-action-organize-imports
            "C-c a" 'eglot-code-actions
            "C-c f" 'eglot-format
            "C-c p" (cape-interactive-capf #'eglot-completion-at-point)))
(use-package eldoc
  :straight (:type built-in)
  :custom
  (eldoc-idle-delay 0.25)
  (eldoc-echo-area-display-truncation-message nil)
  (eldoc-echo-area-use-multiline-p nil)
  (eldoc-echo-area-prefer-doc-buffer nil)
  :general
  ("C-h ." 'eldoc))
(use-package elec-pair
  :straight (:type built-in)
  :preface
  (defun electric-pair-inhibit (char)
    (and (eq char (char-before))
         (eq char (char-before (- (point) 1)))))
  :custom
  (electric-pair-open-newline-between-pairs t)
  (electric-pair-delete-adjacent-pairs t)
  (electric-pair-skip-whitespace t)
  :config
  (electric-pair-mode t))
(use-package embark ;; https://github.com/oantolin/embark
  :custom
  (prefix-help-command embark-prefix-help-command)
  :general
  ("C-." 'embark-act)
  ("C-h b" 'embark-bindings))
(use-package embark-consult ;; https://github.com/oantolin/embark
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))
(use-package eshell-syntax-highlighting ;; https://github.com/akreisher/eshell-syntax-highlighting
  :defer t
  :hook
  (eshell-mode . eshell-syntax-highlighting-mode))
(use-package gcmh ;; https://github.com/emacsmirror/gcmh
  :blackout
  :config
  (gcmh-mode t))
(use-package glsl-mode
  :defer t)
(use-package go-mode ;; https://github.com/dominikh/go-mode.el
  :defer t
  :mode ("\\.go\\'" . go-mode)
  :hook
  (go-mode . eglot-ensure))
(use-package haskell-mode ;; https://github.com/haskell/haskell-mode
  :defer t
  :init
  (defun my/haskell-mode/setup ()
    "Overwrite the stupid tab-width 8 default"
    (setq tab-width 4))
  :mode ("\\.hs\\'" . haskell-mode)
  :custom
  (haskell-indentation-electric-flag t)
  (haskell-indentation-left-offset 4)
  (haskell-indentation-layout-offset 4)
  (haskell-indentation-starter-offset 4)
  (haskell-indentation-where-pre-offset 4)
  (haskell-indentation-where-post-offset 4)
  :hook
  (haskell-mode . haskell-indentation-mode)
  (haskell-mode . my/haskell-mode/setup)
  (haskell-mode . eglot-ensure))
(use-package helpful) ;; https://github.com/Wilfred/helpful
(use-package tempel ;; https://github.com/minad/tempel
  ;; :general
  ;; (:keymaps 'tempel-map
  ;;           "TAB" 'tempel-next
  ;;           "S-TAB" 'tempel-previous)
  :general
  ("M-\\" 'tempel-expand)
  :custom
  (tempel-path "~/.emacs.d/templates"))
(use-package tex-mode ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/TeX-Mode.html
  :straight (:type built-in)
  :defer t
  :custom
  (tex-fontify-script nil)
  :init
  (defun compile-latex ()
    "Compile current buffer with pdflatex"
    (when (eq major-mode 'latex-mode)
      (shell-command-to-string (format "pdflatex --synctex=1 \"%s\"" buffer-file-name))))
  (add-hook 'after-save-hook #'compile-latex))
(use-package tree-sitter ;; https://tree-sitter.github.io/tree-sitter/
  :blackout
  :config
  (global-tree-sitter-mode t)
  :hook
  (tree-sitter-mode . tree-sitter-hl-mode))
(use-package tree-sitter-langs)
(use-package typescript-mode ;; https://github.com/emacs-typescript/typescript.el
  :hook
  (typescript-mode . eglot-ensure))
(use-package ligature ;; https://github.com/mickeynp/ligature.el
  :straight '(ligature :type git
                       :host github
                       :repo "mickeynp/ligature.el")
  :config
  (global-ligature-mode t)
  ;; Enable all Iosevka ligatures in programming modes
  (ligature-set-ligatures 'prog-mode
                          '("-<<" "-<" "-<-" "<--" "<---" "<<-"
                            "<-" "->" "->>" "-->" "--->" "->-"
                            ">->" "<-<" ">~>" "<~<"
                            ">-" ">>-" "=<<" "=<" "=<=" "<=="
                            "<===" "<<=" "<=" "=>" "=>>" "==>"
                            "===>" "=>=" ">=" ">>=" "<->" "<-->"
                            "<--->" "<---->" "<=>" "<==>" "<===>"
                            "<====>" "::" ":::" "__" "<~~" "</"
                            "</>" "/>" "~~>" "==" "!=" "/=" "~="
                            "<>" "===" "!==" "!===" "=/=" "=!="
                            "<:" ":=" ":-" ":+" "<*" "<*>" "*>"
                            "<|" "<|>" "|>" "<." "<.>" ".>" "+:"
                            "-:" "=:" ":>" "(*" "*)" "[|" "|]"
                            "{|" "|}" "++" "+++" "\\/" "/\\" "|-"
                            "-|" "<!--" "<!---" "<***>"
                            ">=>" "<=<" ">>=>>" "<<=<<" "<$>")))
(use-package magit)
(use-package markdown-mode ;; https://github.com/defunkt/markdown-mode
  :defer t
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :custom
  (markdown-fontify-code-blocks-natively t)
  (markdown-command "multimarkdown"))
(use-package marginalia ;; https://github.com/minad/marginalia/
  :config
  (marginalia-mode t))
(use-package meow ;; https://github.com/meow-edit/meow/
  :preface
  (defun meow-setup ()
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-colemak)
    (meow-motion-overwrite-define-key
     ;; Use e to move up, n to move down.
     ;; Since special modes usually use n to move down, we only overwrite e here.
     '("e" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
     '("?" . meow-cheatsheet)
     ;; To execute the originally e in MOTION state, use SPC e.
     '("e" . "H-e")
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument))
    (meow-normal-define-key
     '("0" . meow-expand-0)
     '("1" . meow-expand-1)
     '("2" . meow-expand-2)
     '("3" . meow-expand-3)
     '("4" . meow-expand-4)
     '("5" . meow-expand-5)
     '("6" . meow-expand-6)
     '("7" . meow-expand-7)
     '("8" . meow-expand-8)
     '("9" . meow-expand-9)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("[" . meow-beginning-of-thing)
     '("]" . meow-end-of-thing)
     '("/" . meow-visit)
     '("a" . meow-append)
     '("A" . meow-open-below)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change)
     '("d" . meow-delete)
     '("e" . meow-prev)
     '("E" . meow-prev-expand)
     '("f" . meow-find)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("m" . meow-left)
     '("M" . meow-left-expand)
     '("i" . meow-right)
     '("I" . meow-right-expand)
     '("j" . meow-join)
     '("k" . meow-kill)
     '("l" . meow-line)
     '("L" . meow-goto-line)
     '("h" . meow-mark-word)
     '("H" . meow-mark-symbol)
     '("n" . meow-next)
     '("N" . meow-next-expand)
     '("o" . meow-block)
     '("O" . meow-to-block)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("r" . meow-replace)
     '("s" . meow-insert)
     '("S" . meow-open-above)
     '("t" . meow-till)
     '("u" . meow-undo)
     '("U" . meow-undo-in-selection)
     '("v" . meow-search)
     '("w" . meow-next-word)
     '("W" . meow-next-symbol)
     '("x" . meow-delete)
     '("X" . meow-backward-delete)
     '("y" . meow-save)
     '("z" . meow-pop-selection)
     '("'" . repeat)
     '("<escape>" . ignore)))
  :custom
  (meow-expand-hint-remove-delay 0)
  (meow-use-clipboard t)
  :config
  (meow-setup)
  (meow-global-mode 1))
(use-package move-text ;; https://github.com/emacsfodder/move-text
  :preface
  (defun indent-region-advice (&rest ignored)
    (let ((deactivate deactivate-mark))
      (if (region-active-p)
          (indent-region (region-beginning) (region-end))
        (indent-region (line-beginning-position) (line-end-position)))
      (setq deactivate-mark deactivate)))
  (defun duplicate-line (&rest ignored)
    (interactive)
    (beginning-of-line)
    (kill-line)
    (yank)
    (newline)
    (yank))
  :config
  (advice-add 'move-text-up :after 'indent-region-advice)
  (advice-add 'move-text-down :after 'indent-region-advice)
  :general
  ("M-p" 'move-text-up)
  ("M-n" 'move-text-down)
  ("C-<return>" 'duplicate-line))
(use-package multiple-cursors ;; https://github.com/magnars/multiple-cursors.el
  :general
  ("C-<" 'mc/mark-next-like-this)
  ("C->" 'mc/mark-previous-like-this)
  ("C-|" 'mc/edit-lines)
  (:keymaps 'mc/keymap
        "<return>" nil))
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))
(use-package pdf-tools ;; https://github.com/politza/pdf-tools
  :config
  (pdf-loader-install))
(use-package python-mode
  :straight (:type built-in)
  :defer t
  :hook (python-mode . eglot-ensure))
(use-package rust-mode ;; https://github.com/rust-lang/rust-mode
  :defer t
  :mode ("\\.rs\\'" . rust-mode)
  :hook (rust-mode . eglot-ensure))
(use-package sass-mode ;; https://github.com/nex3/sass-mode
  :custom
  (sass-indent-offset 4))
(use-package smart-comment ;; https://github.com/paldepind/smart-comment
  :general
  ("M-;" 'smart-comment))
(use-package vertico ;; https://github.com/minad/vertico
  :straight (:files (:defaults "extensions/*.el"))
  :init
  (require 'vertico-flat)
  (require 'vertico-multiform)
  (require 'vertico-directory)
  :config
  (vertico-mode t)
  (vertico-flat-mode t)
  (vertico-multiform-mode t)
  :custom
  (enable-recursive-minibuffers t)
  (vertico-cycle t)
  :general
  (:keymaps 'vertico-map
            "M-q" 'vertico-multiform-flat
            "TAB" 'vertico-directory-enter
            "DEL" 'vertico-directory-delete-char
            "M-DEL" 'vertico-directory-delete-word))
(use-package which-key ;; https://github.com/justbur/emacs-which-key
  :blackout which-key-mode
  :custom
  (which-key-idle-delay 0.3)
  :config
  (which-key-mode t))
(use-package yaml-mode ;; https://github.com/yoshiki/yaml-mode
  :defer t
  :custom
  (yaml-indent-offset 4)
  :mode ("\\.yaml\\'" . yaml-mode))
(use-package zig-mode ;; https://github.com/ziglang/zig-mode
  :mode ("\\.zig'" . zig-mode))
;;; Footer:
(provide 'init)
;;; init.el ends here


(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)
